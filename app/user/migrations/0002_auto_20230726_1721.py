# Generated by Django 3.2.20 on 2023-07-26 17:21

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("authtoken", "0003_tokenproxy"),
        ("admin", "0003_logentry_add_action_flag_choices"),
        ("user", "0001_initial"),
    ]

    operations = [
        migrations.DeleteModel(
            name="Admin",
        ),
        migrations.RemoveField(
            model_name="employee",
            name="user_ptr",
        ),
        migrations.DeleteModel(
            name="Employee",
        ),
        migrations.AddField(
            model_name="user",
            name="reset_pass_otp",
            field=models.CharField(blank=True, max_length=6, null=True),
        ),
        migrations.AddField(
            model_name="user",
            name="verify_phone_otp",
            field=models.CharField(blank=True, max_length=6, null=True),
        ),
    ]
