"""
Database models
"""
from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager, PermissionsMixin

from menu.models import Menu


class UserManager(BaseUserManager):
    """Manager for user profiles."""

    """Extra fields are for any other fields that we want to pass in (like name, age, ...)."""

    def create_user(self, phone, password, **extra_fields):
        """Create a new user profile."""
        if not phone:
            raise ValueError("Users must have a phone.")
        user = self.model(phone=phone, name=extra_fields["name"])
        """When using set_password, the password will be hashed before it is stored in the database."""
        user.set_password(password)
        user.save(using=self._db)

        return user

    def create_superuser(self, phone, password, **extra_fields):
        if not phone:
            raise ValueError("Users must have a phone.")
        """Create and save a new superuser with given details."""
        user = self.create_user(phone, password, **extra_fields)
        user.is_staff = True
        user.is_superuser = True
        user.save(using=self._db)

        return user


class User(AbstractBaseUser, PermissionsMixin):
    """Custom user model that supports using phone instead of username."""

    menus = models.ManyToManyField(Menu, blank=True, through="Bill")

    phone = models.CharField(max_length=255, unique=True, default=None)
    name = models.CharField(max_length=255)
    is_active = models.BooleanField(default=True)
    """Can login to the admin site."""
    is_staff = models.BooleanField(default=False)
    verify_phone_otp = models.CharField(max_length=6, blank=True, null=True)
    reset_pass_otp = models.CharField(max_length=6, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    objects = UserManager()

    """Replace default username field comes from django user with phone field."""
    USERNAME_FIELD = "phone"


class Bill(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    menu = models.ForeignKey(Menu, on_delete=models.CASCADE)
    quantity = models.IntegerField(default=1)
    is_paid = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
