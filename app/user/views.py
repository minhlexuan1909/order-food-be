"""
Views for the user API
"""
from rest_framework import generics, authentication, permissions, viewsets, filters, status
from rest_framework.response import Response
from rest_framework.decorators import action
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.settings import api_settings
from rest_framework.authtoken.models import Token

from datetime import datetime, timedelta

from .models import Bill
from user.serializers import (
    SuperUserSerializer,
    UserSerializer,
    AuthTokenSerializer,
    BillSerializer,
    CreateBillsSerializer,
)


class CreateUserView(generics.CreateAPIView):
    """Create a new user in the system"""

    serializer_class = UserSerializer


class CreateSuperUserView(generics.CreateAPIView):
    """Create a new user in the system"""

    serializer_class = SuperUserSerializer


class CreateTokenView(ObtainAuthToken):
    """Create a new auth token for user"""

    serializer_class = AuthTokenSerializer
    renderer_classes = api_settings.DEFAULT_RENDERER_CLASSES

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data["user"]
        token, created = Token.objects.get_or_create(user=user)
        return Response({"token": token.key, "is_superuser": user.is_superuser})


# RetrieveUpdateAPIView: retrieve and update the object
# Retrieve: GET
# Update: PUT, PATCH
class ManageUserView(generics.RetrieveUpdateAPIView):
    """Manage the authenticated user"""

    serializer_class = UserSerializer
    authentication_classes = (authentication.TokenAuthentication,)
    # Set who can access this view (only authenticated user can access)
    permission_classes = (permissions.IsAuthenticated,)

    # Override the default get_object method (GET request)
    def get_object(self):
        """Retrieve and return authentication user"""
        return self.request.user


class BillViewSet(viewsets.ModelViewSet):
    """Manage bills in the database."""

    serializer_class = BillSerializer
    queryset = Bill.objects.all()
    permission_classes = [permissions.IsAuthenticated]
    filter_backends = [filters.OrderingFilter]
    ordering_fields = "__all__"

    def get_serializer_class(self):
        """Return the serializer class for request"""
        if self.action == "create_bills":
            return CreateBillsSerializer
        return self.serializer_class

    def get_queryset(self):
        queryset = self.queryset
        name = self.request.query_params.get("name")
        date = self.request.query_params.get("date")
        if name is not None:
            queryset = queryset.filter(name__unaccent__icontains=name)
        if date is not None:
            converted_time = datetime.strptime(date, "%d-%m-%Y")
            utc_date = converted_time - timedelta(hours=7)
            utc_time_plus_day = utc_date + timedelta(days=1)
            queryset = queryset.filter(created_at__range=[utc_date, utc_time_plus_day])
        if self.action == "list":
            queryset = queryset.filter(user_id=self.request.user.id)
        return queryset.order_by("-created_at")

    def list(self, request, *args, **kwargs):
        return super().list(request, *args, **kwargs)

    @action(methods=["POST"], detail=False, url_path="create-bills")
    def create_bills(self, request, pk=None):
        """Create a new bill"""
        current_time = datetime.utcnow() + timedelta(hours=7)
        current_hour = current_time.hour
        if current_hour >= 8 and current_hour <= 10:
            serializer = self.get_serializer(data=request.data)
            if serializer.is_valid(raise_exception=True):
                res = Bill.objects.bulk_create(
                    [
                        Bill(
                            user_id=request.user.id,
                            menu_id=menu["menu_id"],
                            quantity=menu["quantity"],
                        )
                        for menu in serializer.validated_data["menu_details"]
                    ]
                )
                return Response(BillSerializer(res, many=True).data, status=status.HTTP_201_CREATED)
        else:
            return Response(
                {"message": "You can only order from 8AM to 10AM"},
                status=status.HTTP_403_FORBIDDEN,
            )

    @action(methods=["GET"], detail=False, url_path="admin")
    def get_admin_bills(self, request, pk=None):
        """Get all bills for admin"""
        if not request.user.is_superuser:
            return Response(status=status.HTTP_403_FORBIDDEN)
        queryset = Bill.objects.all().order_by("-created_at")
        name = self.request.query_params.get("name")
        date = self.request.query_params.get("date")
        if name is not None:
            queryset = queryset.filter(name__unaccent__icontains=name)
        if date is not None:
            converted_time = datetime.strptime(date, "%d-%m-%Y")
            utc_date = converted_time - timedelta(hours=7)
            utc_time_plus_day = utc_date + timedelta(days=1)
            queryset = queryset.filter(created_at__range=[utc_date, utc_time_plus_day])
        queryset = self.filter_queryset(queryset)
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)
