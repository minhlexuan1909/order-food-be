"""
Serializers for the user API View
"""
from django.contrib.auth import get_user_model, authenticate
from django.utils.translation import gettext as _
from django.http import JsonResponse

from rest_framework import serializers, exceptions, status
from rest_framework.validators import UniqueValidator

from core.exceptions import GenericAPIException

from .models import Bill
from menu.models import Menu
from menu.serializers import MenuSerializer


class CustomUniquePhoneValidator:
    def __init__(self, queryset):
        self.queryset = queryset

    def __call__(self, value):
        queryset = self.queryset
        if queryset.filter(phone=value).exists():
            raise GenericAPIException(
                detail="Phone number already exists.", status_code=status.HTTP_409_CONFLICT
            )


class UserSerializer(serializers.ModelSerializer):
    """Serializer for the users object"""

    password_confirm = serializers.CharField(
        style={"input_type": "password"}, trim_whitespace=False, write_only=True
    )

    class Meta:
        # Model serializer represents
        model = get_user_model()
        # Fields to be used in the request
        fields = ("phone", "password", "password_confirm", "name", "is_staff", "is_superuser")
        # password is write only, cannot be read, min length is 5 (for validation)
        extra_kwargs = {
            "phone": {
                "required": True,
                "validators": [CustomUniquePhoneValidator(queryset=get_user_model().objects.all())],
            },
            "password": {"write_only": True, "min_length": 5},
            "is_staff": {"read_only": True},
        }

    # Override the default create method
    # The default create method is to create a new object with the validated data, so the password is not encrypted
    # Override it with
    def create(self, validated_data):
        if validated_data["password"] != validated_data["password_confirm"]:
            raise serializers.ValidationError("Passwords must match.")
        """Create a new user with encrypted password and return it"""
        return get_user_model().objects.create_user(**validated_data)

    # Override the default update method (same reason as above)
    def update(self, instance, validated_data):
        """Update a user, setting the password correctly and return it"""
        # Retrive the password from the validated data dict (might contains name, password)
        # Pop: get and remove from the dict (password need to be encrypted before saving, other info can be saved directly)
        password = validated_data.pop("password", None)
        # save here
        # super the default update method from Serializer class
        user = super().update(instance, validated_data)

        # if password is contained in the validated data (means that user want to change password), set the password
        if password:
            user.set_password(password)
            user.save()

        return user


class SuperUserSerializer(UserSerializer):
    def create(self, validated_data):
        if validated_data["password"] != validated_data["password_confirm"]:
            raise serializers.ValidationError("Passwords must match.")
        return get_user_model().objects.create_superuser(**validated_data)


# class


class AuthTokenSerializer(serializers.Serializer):
    """Serializer for the user authentication token"""

    phone = serializers.CharField()
    password = serializers.CharField(style={"input_type": "password"}, trim_whitespace=False)

    def validate(self, attrs):
        """Validate and authenticate the user"""
        phone = attrs.get("phone")
        password = attrs.get("password")
        user = authenticate(
            request=self.context.get("request"),
            username=phone,
            password=password,
        )
        if not user:
            raise serializers.ValidationError("Unable to authenticate with provided credentials")

        attrs["user"] = user
        return attrs


class MenuDetailsSerializer(serializers.Serializer):
    menu_id = serializers.IntegerField()
    quantity = serializers.IntegerField()


class BillSerializer(serializers.ModelSerializer):
    total_price = serializers.SerializerMethodField(read_only=True)
    user = UserSerializer(read_only=True)

    def get_total_price(self, obj):
        total_price = 0
        for menu in Menu.objects.filter(bill=obj):
            menu_data = MenuSerializer(menu).data
            total_price += menu_data["total_price"] * obj.quantity
        return total_price

    class Meta:
        model = Bill
        fields = "__all__"
        depth = 2


class CreateBillsSerializer(serializers.Serializer):
    menu_details = serializers.ListField(
        child=MenuDetailsSerializer(), write_only=True, required=True
    )
