from django.db import models

from food.models import Food


# Create your models here.
class Menu(models.Model):
    foods = models.ManyToManyField(Food, through="MenuFood")
    name = models.CharField(max_length=255)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


class MenuFood(models.Model):
    menu = models.ForeignKey(Menu, on_delete=models.CASCADE)
    food = models.ForeignKey(Food, on_delete=models.CASCADE)
    quantity = models.IntegerField(default=1)
    unit_price = models.IntegerField(default=0)


class MenuDetail(object):
    def __init__(self, name, menu, foods):
        self.name = name
        self.menu = menu
        self.foods = foods
