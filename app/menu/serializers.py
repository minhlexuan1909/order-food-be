from rest_framework.fields import empty
from .models import Menu, MenuFood
from core.exceptions import GenericAPIException
from core.permissions import IsAdminOrReadOnly
from food.models import Food
from food.serializers import FoodSerializer

from rest_framework import serializers


class MenuFoodSerializer(serializers.ModelSerializer):
    class Meta:
        model = MenuFood
        fields = "__all__"


class FoodInMenuSerializer(serializers.Serializer):
    food_id = serializers.IntegerField(required=True, write_only=True)
    quantity = serializers.IntegerField(required=True)
    unit_price = serializers.IntegerField(required=True)

    class Meta:
        fields = "__all__"


class MenuSerializer(serializers.ModelSerializer):
    food_in_menu = serializers.ListField(
        child=FoodInMenuSerializer(), write_only=True, required=False
    )
    food_menu = serializers.SerializerMethodField()
    total_price = serializers.SerializerMethodField()

    def get_food_menu(self, obj):
        food_menu = []
        for menu_food in MenuFood.objects.filter(menu=obj):
            food_menu.append(
                {
                    "food": FoodSerializer(menu_food.food).data,
                    "quantity": menu_food.quantity,
                    "unit_price": menu_food.unit_price,
                }
            )
        return food_menu

    def get_total_price(self, obj):
        total_price = 0
        for menu_food in MenuFood.objects.filter(menu=obj):
            total_price += menu_food.unit_price * menu_food.quantity
        return total_price

    class Meta:
        model = Menu
        fields = (
            "id",
            "name",
            "food_in_menu",
            "food_menu",
            "total_price",
            "created_at",
            "updated_at",
        )

    def _get_foods(self, food_in_menu, menu):
        for food in food_in_menu:
            try:
                food = dict(food)
                food_obj = Food.objects.get(id=food["food_id"])
                MenuFood.objects.get_or_create(
                    menu=menu,
                    food=food_obj,
                    quantity=food["quantity"],
                    unit_price=food["unit_price"],
                )
            except Food.DoesNotExist:
                raise GenericAPIException(
                    detail="Food with id {} does not exist".format(food["food_id"]), status_code=404
                )

    def create(self, validated_data):
        food_in_menu = validated_data.pop("food_in_menu", [])
        print(food_in_menu)
        menu = Menu.objects.create(**validated_data)
        self._get_foods(food_in_menu, menu)
        return menu

    def update(self, instance, validated_data):
        food_in_menu = validated_data.pop("food_in_menu", [])
        instance.name = validated_data.get("name", instance.name)
        if food_in_menu is not None:
            instance.foods.clear()
            self._get_foods(food_in_menu, instance)
        instance.save()
        return instance
