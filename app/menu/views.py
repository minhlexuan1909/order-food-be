from rest_framework import viewsets, filters, exceptions
from rest_framework import permissions

from datetime import datetime, timedelta

from core.permissions import IsAdminOrReadOnly
from .serializers import MenuSerializer
from .models import Menu

from drf_spectacular.utils import (
    extend_schema,
    extend_schema_view,
    OpenApiParameter,
    OpenApiTypes,
)


@extend_schema_view(
    list=extend_schema(
        parameters=[
            OpenApiParameter(
                name="name",
                type=OpenApiTypes.STR,
                location=OpenApiParameter.QUERY,
                description="Filter by name",
            ),
            OpenApiParameter(
                name="idList",
                type=OpenApiTypes.STR,
                location=OpenApiParameter.QUERY,
                description="Filter by list of id (Comma seperated)",
            ),
            OpenApiParameter(
                name="date",
                type=OpenApiTypes.STR,
                location=OpenApiParameter.QUERY,
                description="Filter by date (dd-mm-yyyy)",
            ),
        ],
    ),
)
class MenuViewSet(viewsets.ModelViewSet):
    """Manage menus in the database."""

    serializer_class = MenuSerializer
    queryset = Menu.objects.all()
    permission_classes = [IsAdminOrReadOnly]
    filter_backends = [filters.OrderingFilter]
    ordering_fields = "__all__"

    def _params_to_ints(self, qs):
        """Convert a list of string IDs to a list of integers"""
        return [int(str_id) for str_id in qs.split(",")]

    def get_queryset(self):
        queryset = self.queryset
        name = self.request.query_params.get("name")
        idList = self.request.query_params.get("idList")
        date = self.request.query_params.get("date")
        if name is not None:
            queryset = queryset.filter(name__unaccent__icontains=name)
        if idList:
            idList_ids = self._params_to_ints(idList)
            queryset = queryset.filter(id__in=idList_ids)
        if date is not None:
            converted_time = datetime.strptime(date, "%d-%m-%Y")
            utc_date = converted_time - timedelta(hours=7)
            utc_time_plus_day = utc_date + timedelta(days=1)
            queryset = queryset.filter(created_at__range=[utc_date, utc_time_plus_day])
        """Return objects for the current authenticated user only."""
        return queryset.order_by("-created_at")
