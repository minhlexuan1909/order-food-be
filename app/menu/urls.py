"""
URL mappings for the product app
"""

from django.urls import path, include

from rest_framework.routers import DefaultRouter

from .views import MenuViewSet

router = DefaultRouter()
router.register("menus", MenuViewSet)

app_name = "menu"

urlpatterns = [
    path("", include(router.urls)),
]
