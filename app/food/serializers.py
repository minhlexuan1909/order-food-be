from .models import Food

from rest_framework import serializers


class FoodSerializer(serializers.ModelSerializer):
    image_server_path = serializers.SerializerMethodField(
        read_only=True, method_name="get_image_server_path"
    )

    def get_image_server_path(self, obj):
        if obj.image:
            return obj.image.url
        return None

    class Meta:
        model = Food
        fields = "__all__"
