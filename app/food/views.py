from django.shortcuts import render
from rest_framework import viewsets, filters, exceptions

from datetime import datetime, timedelta

from core.permissions import IsAdminOrReadOnly


from .models import Food
from .serializers import FoodSerializer

from drf_spectacular.utils import (
    extend_schema,
    extend_schema_view,
    OpenApiParameter,
    OpenApiTypes,
)


@extend_schema_view(
    list=extend_schema(
        parameters=[
            OpenApiParameter(
                name="name",
                type=OpenApiTypes.STR,
                location=OpenApiParameter.QUERY,
                description="Filter by name",
            ),
        ],
    ),
)
class FoodViewSet(viewsets.ModelViewSet):
    """Manage foods in the database."""

    serializer_class = FoodSerializer
    queryset = Food.objects.all()
    permission_classes = (IsAdminOrReadOnly,)
    filter_backends = [filters.OrderingFilter]
    ordering_fields = "__all__"

    def get_queryset(self):
        queryset = self.queryset
        name = self.request.query_params.get("name")
        date = self.request.query_params.get("date")
        if name is not None:
            queryset = queryset.filter(name__unaccent__icontains=name)
        if date is not None:
            converted_time = datetime.strptime(date, "%d-%m-%Y")
            utc_date = converted_time - timedelta(hours=7)
            utc_time_plus_day = utc_date + timedelta(days=1)
            queryset = queryset.filter(created_at__range=[utc_date, utc_time_plus_day])
        """Return objects for the current authenticated user only."""
        return queryset.filter(is_active=True).order_by("-created_at")
