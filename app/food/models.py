from django.db import models
import uuid
import os


def product_image_file_path(instance, filename):
    """Generate file path for new product image"""
    # Extract file extension from filename (.png, .jpg, etc.)
    print("filename", filename)
    ext = os.path.splitext(filename)[1]
    filename = f"{uuid.uuid4()}.{ext}"

    return os.path.join("uploads", "food", filename)


# Create your models here.
class Food(models.Model):
    name = models.CharField(max_length=255)
    image = models.ImageField(upload_to=product_image_file_path, null=True, blank=True)
    description = models.TextField()
    default_price = models.IntegerField(default=0)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    is_active = models.BooleanField(default=True)
