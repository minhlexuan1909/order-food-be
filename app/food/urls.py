"""
URL mappings for the product app
"""

from django.urls import path, include

from rest_framework.routers import DefaultRouter

from .views import FoodViewSet

router = DefaultRouter()
router.register("foods", FoodViewSet)

app_name = "food"

urlpatterns = [
    path("", include(router.urls)),
]
