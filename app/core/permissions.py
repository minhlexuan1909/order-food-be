from rest_framework import permissions


class IsAdminOrReadOnly(permissions.BasePermission):
    """Allow unsafe methods for admin users only."""

    def has_permission(self, request, view):
        if bool(not request.user or not request.user.is_authenticated):
            return False
        if bool(request.user.is_staff or request.user.is_superuser):
            return True
        if bool(request.method in permissions.SAFE_METHODS):
            return True
        return False
